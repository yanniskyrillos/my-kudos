An application for sharing kudos (praise for achievements, team efforts etc) 
among groups of people. Currently in progress to provide multiple log-in options. 
The log-in functionality is achieved through OAuth2, storing state server-side.
Next steps will be replacing server state with a JWT and then the addition of 
a client-side application (using either Angular or React).