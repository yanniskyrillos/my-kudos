package com.agileactors.kudosapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KudosAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(KudosAppApplication.class, args);
	}
}
