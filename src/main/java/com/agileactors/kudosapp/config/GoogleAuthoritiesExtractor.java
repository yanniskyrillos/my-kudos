package com.agileactors.kudosapp.config;

import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import java.util.List;
import java.util.Map;

public class GoogleAuthoritiesExtractor implements AuthoritiesExtractor {

    @Override
    public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
        String email = (String) map.get("email");
        return authorizeAgileActorsAndAdmins(email);
    }

    private List<GrantedAuthority> authorizeAgileActorsAndAdmins(String email) {
        if (email.equals("yannis.kyrillos@agileactors.com")) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN");
        }
        if (email.endsWith("@agileactors.com") || email.equals("kyrillosyannis@gmail.com")) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
        } else {
            throw new BadCredentialsException("Email not allowed. Try your Agile Actors account.");
        }
    }
}
