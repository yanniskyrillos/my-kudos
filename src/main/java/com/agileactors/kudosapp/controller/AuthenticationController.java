package com.agileactors.kudosapp.controller;

import com.agileactors.kudosapp.util.LoggedInUserHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping
public class AuthenticationController {

    @Autowired
    LoggedInUserHolder loggedInUserHolder;

    @GetMapping(value = "/authorities")
    public List<String> getRole() {
        Authentication authentication = loggedInUserHolder.getCurrentAuthentication();
        return authentication.getAuthorities()
                .stream()
                .map(o -> o.toString())
                .collect(Collectors.toList());
    }
}
