package com.agileactors.kudosapp.controller;

import com.agileactors.kudosapp.dto.KudosDto;
import com.agileactors.kudosapp.service.KudosService;
import com.agileactors.kudosapp.util.LoggedInUserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping
@Slf4j
public class KudosController {

    @Autowired
    KudosService kudosService;

    @Autowired
    LoggedInUserHolder loggedInUserHolder;

    @PostMapping(value = "/user/kudos")
    public ResponseEntity create(@RequestBody KudosDto kudosDto) {
        log.info("Saving kudos for receiver: {} started", kudosDto.getReceiver());
        kudosService.save(kudosDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/admin/kudos")
    public List<KudosDto> findAll() {
        log.info("Finding all kudos...");
        return kudosService.findAll();
    }

    @GetMapping(value = "/user/kudos")
    public List<KudosDto> findByVoter() {
        String voter = loggedInUserHolder.getCurrentUserName();
        log.info("Finding kudos by voter: {} ", voter);
        return kudosService.findByVoter(voter);
    }

    @GetMapping(value = "/admin/kudos/{date}")
    public List<KudosDto> findByDate(@PathVariable(name = "date") String date) {
        log.info("Finding kudos by date: {} and earlier", date);
        return kudosService.findByDate(date);
    }

    @DeleteMapping(value = "/admin/kudos" )
    public ResponseEntity delete(@RequestBody List<Long> ids) {
        log.info("Deleting kudos by given ids");
        kudosService.delete(ids);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
