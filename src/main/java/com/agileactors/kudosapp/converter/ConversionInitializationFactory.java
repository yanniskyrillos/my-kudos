package com.agileactors.kudosapp.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class ConversionInitializationFactory {

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private List<Converter> converters;

    @PostConstruct
    void init() {
        for (Converter converter : converters) {
            ((GenericConversionService) conversionService).addConverter(converter);
        }
    }
}
