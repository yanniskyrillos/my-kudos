package com.agileactors.kudosapp.converter;

import com.agileactors.kudosapp.domain.Kudos;
import com.agileactors.kudosapp.dto.KudosDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class KudosDtoToKudosConverter implements Converter<KudosDto, Kudos> {

    @Override
    public Kudos convert(KudosDto kudosDto) {
        return Kudos
                .builder()
                .text(kudosDto.getText())
                .receiver(kudosDto.getReceiver())
                .build();
    }
}
