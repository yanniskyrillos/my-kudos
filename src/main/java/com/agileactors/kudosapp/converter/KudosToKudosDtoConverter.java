package com.agileactors.kudosapp.converter;

import com.agileactors.kudosapp.domain.Kudos;
import com.agileactors.kudosapp.dto.KudosDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class KudosToKudosDtoConverter implements Converter<Kudos, KudosDto> {

    @Override
    public KudosDto convert(Kudos kudos) {
        return KudosDto
                .builder()
                .id(kudos.getId())
                .text(kudos.getText())
                .receiver(kudos.getReceiver())
                .voter(kudos.getVoter())
                .build();
    }
}



