package com.agileactors.kudosapp.domain;

import lombok.*;
import javax.persistence.*;
import java.time.Instant;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "KUDOS_APPLICATION")
public class Kudos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "voter")
    private String voter;

    @Column(name = "text")
    private String text;

    @Column(name = "receiver")
    private String receiver;

    @Column(name = "date")
    private Instant date;

}
