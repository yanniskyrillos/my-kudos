package com.agileactors.kudosapp.dto;

import lombok.*;

@Setter
@ToString
@EqualsAndHashCode
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class KudosDto {

    private Long id;
    private String text;
    private String voter;
    private String receiver;
}
