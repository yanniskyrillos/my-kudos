package com.agileactors.kudosapp.repository;

import com.agileactors.kudosapp.domain.Kudos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface KudosRepository extends JpaRepository<Kudos, Long> {

    Optional<Kudos> findById(Long id);

    List<Kudos> findAll();

    List<Kudos> findByVoter(String voter);

    List<Kudos> findByDateLessThanEqual(Instant date);

    @Transactional
    void deleteByIdIn(List<Long> ids);


    //the following deletes using only one query, avoiding the initial select that Hibernate uses
    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "DELETE FROM KUDOS WHERE ID IN (:ids)")
    void deleteByListId(@Param("ids") List<Long> ids);


}
