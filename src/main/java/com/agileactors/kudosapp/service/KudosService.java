package com.agileactors.kudosapp.service;

import com.agileactors.kudosapp.domain.Kudos;
import com.agileactors.kudosapp.dto.KudosDto;
import com.agileactors.kudosapp.repository.KudosRepository;
import com.agileactors.kudosapp.util.LoggedInUserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class KudosService {

    private static final String DATE_PATTERN = "dd/MM/yyyy";

    @Autowired
    KudosRepository kudosRepository;

    @Autowired
    ConversionService conversionService;

    @Autowired
    LoggedInUserHolder loggedInUserHolder;

    public Kudos save(KudosDto kudosDto) {
        log.info("Saving kudos for receiver: {} started", kudosDto.getReceiver());
        Kudos kudos = conversionService.convert(kudosDto, Kudos.class);
        String voter = loggedInUserHolder.getCurrentUserName();
        kudos.setVoter(voter);
        kudos = kudosRepository.save(kudos);
        log.info("Saving kudos for receiver: {} end", kudosDto.getReceiver());
        return kudos;
    }

    public List<KudosDto> findAll() {
        log.info("Finding all kudos start");
        List<Kudos> kudos = kudosRepository.findAll();
        List<KudosDto> kudosDtos = kudos
                .stream()
                .map(kudo -> conversionService.convert(kudo, KudosDto.class))
                .collect(Collectors.toList());
        log.info("Finding all kudos end");
        return kudosDtos;
    }

    public List<KudosDto> findByVoter(String voter) {
        log.info("Finding kudos written by user: {} start", voter);
        List<Kudos> kudos = kudosRepository.findByVoter(voter);
        List<KudosDto> kudosDtos = kudos
                .stream()
                .map(kudo -> conversionService.convert(kudo, KudosDto.class))
                .collect(Collectors.toList());
        log.info("Finding all kudos by user: {} end", voter);
        return kudosDtos;
    }

    public void delete(List<Long> ids) {
        log.info("Deleting kudos by given ids start");
        kudosRepository.deleteByIdIn(ids);
        log.info("Deleting kudos by given ids end");
    }

    public List<KudosDto> findByDate(String date) {
        log.info("Finding kudos from date {} and earlier", date);
        Instant instant = buildInstantFromString(date);
        List<Kudos> kudos = kudosRepository.findByDateLessThanEqual(instant);
        List<KudosDto> kudosDtos = kudos
                .stream()
                .map(kudo -> conversionService.convert(kudo, KudosDto.class))
                .collect(Collectors.toList());
        log.info("Finding kudos from date {} and earlier end", date);
        return kudosDtos;
    }

    private Instant buildInstantFromString(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        Instant instant = Instant.from(formatter.parse(date));
        return instant;
    }
}
