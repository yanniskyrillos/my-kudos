package com.agileactors.kudosapp.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class LoggedInUserHolder {

    public String getCurrentUserName() {
        Authentication authentication = SecurityContextHolder
                .getContext()
                .getAuthentication();
        return authentication.getName();
    }

    public Authentication getCurrentAuthentication() {
        Authentication authentication = SecurityContextHolder
                .getContext()
                .getAuthentication();
        return authentication;
    }
}
