import com.agileactors.kudosapp.domain.Kudos
import com.agileactors.kudosapp.dto.KudosDto
import com.agileactors.kudosapp.repository.KudosRepository
import com.agileactors.kudosapp.service.KudosService
import com.agileactors.kudosapp.util.LoggedInUserHolder
import org.springframework.core.convert.ConversionService
import spock.lang.Specification

class KudosServiceSpec extends Specification {

    KudosService service = new KudosService()

    def setup() {
        service.conversionService = Mock(ConversionService)
        service.kudosRepository = Mock(KudosRepository)
        service.loggedInUserHolder = Mock(LoggedInUserHolder)
    }

    def "find by voter"() {
        given: "there is one result in the database"
            Kudos kudos = Kudos
                    .builder()
                    .text("kudos for a good job")
                    .receiver("to this guy")
                    .voter("voter")
                    .build()
        when: "the method is called"
            List<KudosDto> result = service.findByVoter("voter")
        then: "the following methods are called once each & the returned list size is 1"
            1 * service.kudosRepository.findByVoter("voter") >> Arrays.asList(kudos)
            1 * service.conversionService.convert(_, KudosDto.class)
            result.size().equals(1)
    }
}